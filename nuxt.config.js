export default {
    ssr: false,
    head: {
        title: '오늘 뭐먹을까?',
        htmlAttrs: {
            lang: 'ko'
        },
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: ''},
            {name: 'format-detection', content: 'telephone=no'}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
        ]
    },

    css: [
        'element-ui/lib/theme-chalk/index.css'
    ],

    plugins: [
        '@/plugins/element-ui'
    ],

    components: true,
    buildModules: [],

    modules: [
        '@nuxtjs/axios',
    ],

    build: {
        transpile: [/^element-ui/],
    }
}
